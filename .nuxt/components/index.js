export { default as Bar } from '../../components/Bar.vue'
export { default as ContactForm } from '../../components/ContactForm.vue'
export { default as Logo } from '../../components/Logo.vue'
export { default as VuetifyLogo } from '../../components/VuetifyLogo.vue'

export const LazyBar = import('../../components/Bar.vue' /* webpackChunkName: "components/bar" */).then(c => c.default || c)
export const LazyContactForm = import('../../components/ContactForm.vue' /* webpackChunkName: "components/contact-form" */).then(c => c.default || c)
export const LazyLogo = import('../../components/Logo.vue' /* webpackChunkName: "components/logo" */).then(c => c.default || c)
export const LazyVuetifyLogo = import('../../components/VuetifyLogo.vue' /* webpackChunkName: "components/vuetify-logo" */).then(c => c.default || c)
