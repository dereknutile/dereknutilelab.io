const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    extend: {
      colors: {
        primary: defaultTheme.colors.green
      }
    },
    container: {
      center: true,
    },
  },
  variants: {},
  plugins: []
}